#include <stdio.h>
#include <stdlib.h>

#include "lista.h"

typedef struct hash Hash;

struct hash {
    int keysSize;
    int hashSize;
    List **hashArray;
};

int hashCode(int key, Hash* hash) {
   return key % hash->keysSize;
}

Hash *createHash(int size) {
	int i;

	if( size < 1 ) return NULL;
    
    Hash *hash = malloc( sizeof(hash));
	hash->keysSize = size/2;
    hash->hashSize = size;

    hash->hashArray = malloc( sizeof(List *) * hash->keysSize);

	for( i = 0; i < hash->keysSize; i++ ) {
		hash->hashArray[i] = NULL;
	}

	return hash;
}

int getSize(Hash *hash) {
    return hash->hashSize;
}

void insert(Hash *hash, int val) {
    int key = hashCode(val, hash);
    if (hash->hashArray[key] != NULL && findInList(hash->hashArray[key], val) != NULL) {
        printf("valor já inserido");
        return;
    }
    hash->hashArray[key] = insertInList(hash->hashArray[key], val);
}

void find(int val, Hash *hash) {
    int key = hashCode(val, hash);

    if (hash->hashArray[key] == NULL) {
        printf("\nValor %d não encontrado\n", val);
        return;
    }

    List *bucket = findInList(hash->hashArray[key], val);
    if (bucket == NULL) {
        printf("\nValor %d não encontrado\n", val);
        return;
    }

    printf("\nvalor %d encontrado\n", val);
}

void removeOfHash(int val, Hash *hash) {
    int key = hashCode(val, hash);

    if (hash->hashArray[key] == NULL) {
        printf("\nValor %d não encontrado\n", val);
        return;
    }

    hash->hashArray[key] = removeItem(hash->hashArray[key], val);
}

void freeHash(Hash *hash) {
    for (int i =0; i< hash->keysSize; i++){
        hash->hashArray[i] = freeList(hash->hashArray[i]);
    }

    free(hash);
    hash = NULL;
}

void printHash(Hash *hash){
    for(int i = 0;i < hash->keysSize; i++)
    {
        if (hash->hashArray[i] != NULL) {
            printf("\n'Hash key': '%d'\n'hash bucket':[\n", i);
            printListInInsertionOrder(hash->hashArray[i]);
            printf("\n]\n");
        }
    }
}