#ifndef LISTA_H_
#define LISTA_H_

typedef struct list List;

List* createList();

List* insertInList(List* list, int val);

void printListInInsertionOrder(List* list);

List* findInList(List* list, int val);

List* removeItem(List* list, int val);

List* freeList(List* list);

#endif